from flask import Flask
from apps import create_app
from flask_cors import CORS,cross_origin     # 解决跨域问题

# apps组件
app = create_app()
CORS(app, supports_credentials=True, resources=r'/*')  # 解决跨域问题

if __name__ == '__main__':
    # 运行
    app.run(host='127.0.0.1', port=5555, threaded=True)
