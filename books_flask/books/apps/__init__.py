# -*- coding = utf-8 -*-
#@Time: 2021/7/3 17:50
#@Author: 卜白
#@File: __init__.py.py
#@Software: PyCharm

import redis
import settings
from flask import Flask
from apps.utils.common import ReConverter   # 正则转换器
from flask_wtf import CSRFProtect
# 蓝图视图
from apps.fictions.view import fiction
from apps.users.view import users

# 创建redis连接对象
redis_store = None

# 配置日志信息
import logging
from logging.handlers import RotatingFileHandler
# 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024*1024*100, backupCount=10)
# 创建日志记录的格式                 日志等级    输入日志信息的文件名 行数    日志信息
formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
# 为刚创建的日志记录器设置日志记录格式
file_log_handler.setFormatter(formatter)
# 为全局的日志工具对象（flask app使用的）添加日记录器
logging.getLogger().addHandler(file_log_handler)
# 设置日志的记录等级
logging.basicConfig(level=logging.DEBUG)  # 调试debug级

# config = {
#     'CACHE_TYPE': 'redis',   # 缓存类型
#     'CACHE_REDIS_HOST': '127.0.0.1',
#     'CACHE_REDIS_PORT': 6379
# }

def create_app():
    app = Flask(__name__)
    # 加载配置文件
    app.config.from_object(settings.DevelopmentConfig)  # 开发模式

    # 初始化配置db
    # db.init_app(app=app)

    # 为flask补充csrf防护
    # CSRFProtect(app)

    # 初始化redis工具
    global redis_store
    # 普通redis连接
    # redis_store = redis.StrictRedis(host=settings.Config.REDIS_HOST, port=settings.Config.REDIS_PORT)
    # 创建redis连接池（默认连接池最大连接数 2**31=2147483648），max_connections最大连接数
    redis_pool= redis.ConnectionPool(host=settings.Config.REDIS_HOST, port=settings.Config.REDIS_PORT, encoding="utf-8", max_connections=1000)

    redis_store = redis.Redis(connection_pool=redis_pool)

    # 为flask添加自定义的转换器
    app.url_map.converters['re'] = ReConverter

    # # 注册蓝图
    app.register_blueprint(fiction)
    app.register_blueprint(users)

    # print(app.url_map)

    return app

