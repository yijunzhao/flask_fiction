# -*- coding = utf-8 -*-
#@Time: 2021/7/18 9:33
#@Author: 卜白
#@File: constants.py
#@Software: PyCharm

# 图片验证码的redis有效期，单位：秒
IMAGE_CODE_REDIS_EXPIRES = 180

# 短信验证码的redis有效期，单位：秒
SMS_CODE_REDIS_EXPIRES = 300