# -*- coding = utf-8 -*-
#@Time: 2021/7/19 20:27
#@Author: 卜白
#@File: common.py
#@Software: PyCharm

from werkzeug.routing import BaseConverter

# 定义正则转换器
class ReConverter(BaseConverter):
    ''''''
    def __init__(self,url_map,regex):
        # 调用父类的初始化方法
        super(ReConverter,self).__init__(url_map)
        # 保存正则表达式
        self.regex = regex