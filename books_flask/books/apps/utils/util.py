# -*- coding = utf-8 -*-
#@Time: 2021/7/17 16:07
#@Author: 卜白
#@File: util.py
#@Software: PyCharm

# 公共方法
import apps
import random
from apps import constants
from random import randint
from flask import jsonify,current_app
from PIL import Image, ImageFont, ImageDraw, ImageFilter

from celery_tasks.sms.yuntongxun.sms import CCP

def get_random_color():
    return (random.randint(0,255),random.randint(0,25),random.randint(0,255))

'''图片验证码'''
def generate_image(length):
    s = 'abCDEFcdefghijk0123lmnopqrHIJKstu465vwxy789zABGUVWXYZ'
    size = (130,50)
    # 创建画布
    im = Image.new('RGB',size,color='#ccc') # color画布背景颜色
    # 创建字体
    font = ImageFont.truetype('FZSTK.TTF',size=35)
    # 创建ImageDraw对象
    draw = ImageDraw.Draw(im)
    # 绘制验证码
    code = ''
    for i in range(length):
        c = random.choice(s)
        code += c
        # x=5+random.randint(4,7)+20*i   y=random.randint(4,7)
        # text：绘制的内容    fill：颜色     font字体
        draw.text((5+random.randint(4,7)+20*i,random.randint(4,7)),
                  text=c,
                  fill=get_random_color(), # 字体颜色
                  font=font)
    # print('验证码为：', code)
    # 绘制干扰线
    for i in range(length):
        x1 = random.randint(0,130)
        y1 = random.randint(0,50/2)
        x2 = random.randint(0,130)
        y2 = random.randint(50/2,50)
        draw.line(((x1,y1),(x2,y2)),fill=get_random_color())    # fill干扰线颜色

    # 添加滤镜：BLUR、CONTOUR、EDGE_ENHANC、EDFGE_ENHANCE_MORE、EMBOSS、FIND_EDFES、SHARREN、SMOOTH、SMOOTH_MORE
    im = im.filter(ImageFilter.EDGE_ENHANCE)
    # im.show()
    return im,code

'''短信验证码'''
def SMS_CODE(mobile):
    redis_conn = apps.redis_store   # redis连接对象

    # 来发短信之前先尝试性的去redis中获取此手机号60s内是否发送过短信
    send_flag = redis_conn.get('send_flag_%s' % mobile)
    # 判断是否有发送过的标记
    if send_flag:
        return jsonify(status=405, msg='频繁发送短信')

    # 随机生成一个6位数字作为短信验证码
    sms_code = '%06d' % randint(0, 999999)
    current_app.logger.info(sms_code)
    # 管道技术
    # 创建管道
    pl = redis_conn.pipeline()
    # 将短信验证码存储到redis 以备后期注册时进行验证
    # redis_conn.setex('sms_%s' % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)  # 魔法数字
    pl.setex('sms_code_%s' % mobile, constants.SMS_CODE_REDIS_EXPIRES // 5, sms_code)  # 魔法数字

    # 向redis存储一个标识,标记此手机号60s内已经发过短信
    # redis_conn.setex('send_flag_%s' % mobile, 60, '1')
    pl.setex('send_flag_%s' % mobile, 60, '1')
    # 执行管道
    pl.execute()

    # 利用容联云平台发短信
    # CCP().send_template_sms('接收短信手机号', ['短信验证码', '提示用户短信验证码多久过期单位分钟'], '模板id')
    # CCP().send_template_sms(mobile, [sms_code, constants.SMS_CODE_REDIS_EXPIRES // 60], 1)
    # 使用celery分布式发布容联云短信
    # send_sms_code.delay(mobile, sms_code)

if __name__ == '__main__':
    generate_image(4)