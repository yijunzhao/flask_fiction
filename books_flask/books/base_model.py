# -*- coding = utf-8 -*-
#@Time: 2021/7/17 14:29
#@Author: 卜白
#@File: base_model.py
#@Software: PyCharm

# 模板base-model
import pymysql
from pymysql import connect
from dbutils.pooled_db import PooledDB
from pymysql.cursors import DictCursor  # 返回字典格式
# 配置文件信息
from settings import MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE

class Base_Model(object):
    def __init__(self):     # 创建对象同时要执行的代码
        # # 连接mysql数据库
        # self.conn = connect(
        #     host=MYSQL_HOST,
        #     port=MYSQL_PORT,
        #     user=MYSQL_USER,
        #     password=MYSQL_PASSWORD,
        #     database=MYSQL_DATABASE,
        #     charset='utf8'
        # )
        # self.cursor = self.conn.cursor(DictCursor)  # 使其运行的SQL命令结果返回字典的形式

        # 连接数据库连接池
        POOL = PooledDB(
            creator=pymysql,  # 使用链接数据库的模块
            maxconnections=6,  # 连接池允许的最大连接数，0和None表示不限制连接数
            mincached=2,  # 初始化时，链接池中至少创建的空闲的链接，0表示不创建
            maxcached=5,  # 链接池中最多闲置的链接，0和None不限制
            maxshared=3,
            # 链接池中最多共享的链接数量，0和None表示全部共享。PS: 无用，因为pymysql和MySQLdb等模块的 threadsafety都为1，所有值无论设置为多少，_maxcached永远为0，所以永远是所有链接都共享。
            blocking=True,  # 连接池中如果没有可用连接后，是否阻塞等待。True，等待；False，不等待然后报错
            maxusage=None,  # 一个链接最多被重复使用的次数，None表示无限制
            setsession=[],  # 开始会话前执行的命令列表。如：["set datestyle to ...", "set time zone ..."]
            ping=0,
            # ping MySQL服务端，检查是否服务可用。
            #  如：0 = None = never,
            # 1 = default = whenever it is requested,
            # 2 = when a cursor is created,
            # 4 = when a query is executed,
            # 7 = always
            host=MYSQL_HOST,
            port=MYSQL_PORT,
            user=MYSQL_USER,
            password=MYSQL_PASSWORD,
            charset="utf8",
            db=MYSQL_DATABASE
        )
        self.conn = POOL.connection()
        self.cursor = self.conn.cursor(DictCursor)  # 使其运行的SQL命令结果返回字典的形式

    def __del__(self):  # 释放对象的同时要执行的代码
        self.cursor.close()     # 关闭游标
        self.conn.close()       # 关闭数据库连接对象
