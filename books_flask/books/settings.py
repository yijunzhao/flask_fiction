# -*- coding = utf-8 -*-
#@Time: 2021/7/3 17:50
#@Author: 卜白
#@File: settings.py
#@Software: PyCharm

# 配置文件
import redis

MYSQL_HOST = '127.0.0.1'        # 地址
MYSQL_PORT = 3306               # 端口号
MYSQL_USER = 'root'             # 用户名
MYSQL_PASSWORD = 'cjs122374'    # 密码
MYSQL_DATABASE = 'books'        # 链接的数据库

class Config:
    DEBUG = True
    # # mysql+pymysql://user:password@hostip:port/databasename
    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:cjs122374@127.0.0.1:3306/flask_blog'
    # # 去除警告
    # SQLALCHEMY_TRACK_MODIFICATIONS = False
    # SQLALCHEMY_ECHO = True
    # secret_key
    SECRET_KEY = 'cjs122374'    # session底层封装的key值，键固定，值随意定义

    # redis
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    # flask-session配置
    SESSION_TYPE = "redis"
    SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)

class DevelopmentConfig(Config):
    ENV = 'development'  # 环境

class ProductionConfig(Config):
    ENV = 'production'
    DEBUG = False