# flask后端

## 1.解决请求跨域问题
方法一：安装
pip install flask_cors
pycharm 安装  flask-cors
初始化的时候加载配置，这样就可以支持跨域访问了

from flask_cors import CORS

app = Flask(__name__)
CORS(app, supports_credentials=True)

if __name__ == "__main__":
    app.run()
    
方法二：
对请求的Response header中加入header

@app.after_request
def af_request(resp):     
    """
     #请求钩子，在所有的请求发生后执行，加入headers。
    :param resp:
    :return:
    """
    resp = make_response(resp)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Access-Control-Allow-Methods'] = 'GET,POST'
    resp.headers['Access-Control-Allow-Headers'] = 'x-requested-with,content-type'
    return resp
    
## 2.mysql默认创建时间以及更新时间
字段类型设为：TIMESTAMP 
默认创建时间：default CURRENT_TIMESTAMP
默认更新时间：default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
mysql5.6.5之前出现报错：
    表定义不正确；在 DEFAULT 或 ON UPDATE 子句中只能有一个带有 CURRENT_TIMESTAMP 的 TIMESTAMP 列
    
解决方法一：将mysql版本升至5.6及以上即可
解决方法二：为字段创建一个触发器，例：CREATE TRIGGER `update_activity_trigger` BEFORE UPDATE ON `activity`
 FOR EACH ROW SET NEW.`activity_starttime` = NOW()
 解决方法三：
    create_time TIMESTAMP DEFAULT '0000-00-00 00:00:00',
    update_time TIMESTAMP DEFAULT now() on update now() 
    注意：在 "插入" 期间必须在两列中插入null值
    insert into test_table(stamp_created, stamp_updated) values(null, null); 


## 3.flask后端使用原生sql语句加密用户密码
from werkzeug.security import generate_password_hash, check_password_hash

generate_password_hash(password)：加密为128位的哈希值
check_password_hash(password_hash,password)：进行密码校验，返回布尔值True\False

## 4.创建用户表以及书架表
用户表：
CREATE TABLE book_users(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(16) NOT NULL,
    `password` VARCHAR(128) NOT NULL,
    phone VARCHAR(11),
    email VARCHAR(30),
    icon VARCHAR(100),
    isdelete TINYINT(1) NOT NULL DEFAULT 0,
    create_time TIMESTAMP DEFAULT '0000-00-00 00:00:00',
    update_time TIMESTAMP DEFAULT NOW() ON UPDATE NOW() 
); 
书架表：
CREATE TABLE book_selfs(
    id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_id INT(10) NOT NULL,
    book_id INT(10) NOT NULL
);

## 5.解决跨域问题出现的不同session问题
后端设置：
@app.after_request
def af_req(resp):  #解决跨域session丢失
    resp = make_response(resp)
    resp.headers['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    resp.headers['Access-Control-Allow-Methods'] = 'PUT,POST,GET,DELETE,OPTIONS'
    #resp.headers['Access-Control-Allow-Headers'] = 'x-requested-with,content-type'
    resp.headers['Access-Control-Allow-Headers'] = 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild'
    resp.headers['Access-Control-Allow-Credentials'] = 'true'

    resp.headers['X-Powered-By'] = '3.2.1'
    resp.headers['Content-Type'] = 'application/json;charset=utf-8'
    return resp

注意：Access-Control-Allow-Origin，源要是你的前端ip地址，不能使用通配（特别注意前端网址localhost与127.0.0.1区别问题）；Access-Control-Allow-Credentials设置为True

前端配置文件设置：axios.defaults.withCredentials = true 
此时在后台两次请求获取的sessionId完全相同，也就是同一个session