# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class BookInfosItem(scrapy.Item):
    # define the fields for your item here like:
    bid = scrapy.Field()
    cid = scrapy.Field()
    aid = scrapy.Field()
    did = scrapy.Field()
    sid = scrapy.Field()
    book_name = scrapy.Field()
    created_time = scrapy.Field()
    update_time = scrapy.Field()
    newest_name = scrapy.Field()
    book_desc = scrapy.Field()
    img_url = scrapy.Field()
    detail_url = scrapy.Field()

class AuthorItem(scrapy.Item):
    author_name = scrapy.Field()

class BookDetailsItem(scrapy.Item):
    bid = scrapy.Field()
    did = scrapy.Field()
    sid = scrapy.Field()
    detail_title = scrapy.Field()
    detail_contents = scrapy.Field()
