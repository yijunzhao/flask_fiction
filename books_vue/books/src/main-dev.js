import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 导入组件库
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
import { Button, Form, FormItem, Input, Card, Row, Col, Table, TableColumn, Message, Pagination } from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Card)
Vue.use(Row)
Vue.use(Col)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Pagination)
Vue.prototype.$message = Message


// 导入全局样式表
import './assets/css/global.css'
// 导入字体图标
import './assets/fonts/iconfont.css'

// 导入 NProgress 包对应的JS和CSS    用户访问显示进度条的第三方包
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

import axios from 'axios'
// 配置请求的跟路径
axios.defaults.baseURL = 'http://127.0.0.1:5555/'
// 解决跨域出现的session不同问题 设置为true
axios.defaults.withCredentials = true

// request请求拦截器  use挂载
axios.interceptors.request.use(config => {
  // 在 request 拦截器中，展示进度条 NProgress.start()
  NProgress.start()
  // console.log(config + '请求拦截器')
  // config.headers.Authorization = window.sessionStorage.getItem('token')
  // 最后必须 return config
  return config
})

// 在 response 拦截器中，隐藏进度条 NProgress.done()
axios.interceptors.response.use(config => {
  setTimeout(() => {
    NProgress.done()
  }, 1000)
  return config
})

// router.beforeEach((to, from, next) => {
//   NProgress.start()
//   next()
// })
// router.afterEach(() => {
//   NProgress.done()
// })

Vue.prototype.$http = axios

Vue.config.productionTip = false

// 制定一个全局的时间过滤器
// Vue.filter('dateFormat', function(originVal) {
//   const dt = new Date(originVal)

//   const y = dt.getFullYear()
//   // padStart(2,'0')：若为一位数则补充为两位数
//   const m = (dt.getMonth() + 1 + '').padStart(2,'0')  // 月份从0开始 需要+1
//   const d = (dt.getDate() + '').padStart(2,'0')

//   const hh = (dt.getHours() + '').padStart(2,'0')
//   const mm = (dt.getMinutes() + '').padStart(2,'0')
//   const ss = (dt.getSeconds() + '').padStart(2,'0')

//   return `${y}-${m}-${d} ${hh}-${mm}-${ss}`
// })

// 制定去空过滤器
Vue.filter('StrStrip', function (strval) {
  // console.log(typeof strval)
  return strval.replace(/^\s+|\s+$/g, "");
})

new Vue({
  router,  // 路由
  store,   // vuex全局
  render: h => h(App)
}).$mount('#app')
