import Vue from 'vue'
import Router from 'vue-router'

// 导入组件
import Base from '../views/Base.vue'
import Index from '../views/fiction/Index.vue'
import Fiction from '../views/fiction/Fiction.vue'
import Detail from '../views/fiction/Detail.vue'
import Chapter from '../views/fiction/Chapter.vue'
import Search from '../views/fiction/Search.vue'
import Library from '../views/fiction/Library.vue'
import BookSelf from '../views/fiction/BookSelf.vue'

Vue.use(Router)

const router = new Router({
  // 路由规则
  routes: [
    // redirect重定向
    { path: '/', redirect: '/base' },
    {
      path: '/base', component: Base, redirect: '/index',
      children: [
        { path: '/index', component: Index },
        { path: '/fiction/:name', component: Fiction },
        { path: '/book/:id', component: Detail },
        { path: '/chapter/:book_id/:sort_id', component: Chapter },
        { path: '/search/:key', component: Search },
        { path: '/library', component: Library },
        { path: '/bookself', component: BookSelf }
      ]
    }
  ]
})

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = Router.prototype.push
Router.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

export default router
