import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

// 创建store实例
const store = new Vuex.Store({
    state: {
        fiction_cate: {
            玄幻: '/fiction/xuanhuan',
            修真: '/fiction/xiuzhen',
            都市: '/fiction/dushi',
            历史: '/fiction/lishi',
            网游: '/fiction/wangyou',
            科幻: '/fiction/kehuan',
            言情: '/fiction/yanqing',
            其他: '/fiction/qita'
        },
        cate_data: {},
        count: 0
    },
    // 只有 mutations 中定义的函数，才有权利修改 state 中的数据
    // 不要在 mutations 函数中，执行异步操作
    mutations: {
        // 获取分类小说 N本数据
        async get_cate_data(state, step) {
            const result = await axios.get(`get_cate_limit/${step}`)
            state.cate_data = result.data
        }
    },
    // 在 actions 中定义异步操作的函数
    actions: {
        // 获取分类小说 N本数据
        get_book_cate(com, step) {
            com.commit('get_cate_data', step)
        }
    },
    modules: {},
    // 计算属性
    getters: {}
})

export default store